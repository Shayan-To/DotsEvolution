package ks.dotsevolution;

import java.util.Random;

public class Utils
{
    private static final Random internalRandom = new Random();

    public static final float random(float high)
    {
        if ((high == 0.0F) || (high != high))
        {
            return 0.0F;
        }

        float value = 0.0F;
        do
        {
            value = internalRandom.nextFloat() * high;
        } while (value == high);
        return value;
    }

    public static final float random(float low, float high)
    {
        if (low >= high)
        {
            return low;
        }
        float diff = high - low;
        float value = 0.0F;

        do
        {
            value = random(diff) + low;
        } while (value == high);
        return value;
    }

    public static final float randomGaussian()
    {
        return (float) internalRandom.nextGaussian();
    }

    public static final void randomSeed(long seed)
    {
        internalRandom.setSeed(seed);
    }

}
