package ks.dotsevolution;

import static ks.dotsevolution.Utils.random;
import static processing.core.PConstants.PI;

import processing.core.PVector;

public class Brain
{
    PVector[] directions; // series of vectors which get the dot to the goal (hopefully)

    Brain(int size)
    {
        this.directions = new PVector[size];
    }

    // --------------------------------------------------------------------------------------------------------------------------------
    // creates a random vector
    @SuppressWarnings("static-method")
    PVector randomVector()
    {
        PVector r = PVector.fromAngle(random(2 * PI));
        r.mult(random(1.5f));
        return r;
    }

    // --------------------------------------------------------------------------------------------------------------------------------
    // sets all the vectors in directions to a random vector with length 1
    void randomize()
    {
        for (int i = 0; i < this.directions.length; i++)
        {
            this.directions[i] = this.randomVector();
        }
    }

    // --------------------------------------------------------------------------------------------------------------------------------
    // returns a perfect copy of this brain object
    Brain createClone()
    {
        Brain clone = new Brain(this.directions.length);
        for (int i = 0; i < this.directions.length; i++)
        {
            clone.directions[i] = this.directions[i].copy();
        }
        return clone;
    }

    // --------------------------------------------------------------------------------------------------------------------------------
    // mutates the brain by setting some of the directions to random vectors
    void mutate(float mutationRate // chance that any vector in directions gets changed
    )
    {
        for (int i = 0; i < this.directions.length; i++)
        {
            float rand = random(1);
            if (rand < mutationRate)
            {
                // set this direction as a random direction
                this.directions[i] = this.randomVector();
            }
        }
    }
}
