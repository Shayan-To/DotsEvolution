package ks.dotsevolution;

import processing.core.PApplet;

public class Application
        extends PApplet
{
    public static void main(String[] args)
    {
        PApplet.main(Application.class.getName());
    }

    World world = new World(1000, 18); // create a new population with 1000 dots and 10 obstacles

    @Override
    public void settings()
    {
        this.size((int) this.world.screenSize.x, (int) this.world.screenSize.y); // size of the window
    }

    @Override
    public void setup()
    {
        this.frameRate(100); // increase this to make the dots go faster
        Utils.randomSeed(905);
        this.world.init();
    }

    @Override
    public void draw()
    {
        this.background(255);

        if (this.world.allDotsDead())
        {
            println();

            this.world.sortDots(true);
            int bestStep = this.world.dots[0].step;
            boolean isAtGoal = this.world.dots[0].reachedGoal;
            for (int i = 0; i < 10; i++)
            {
                print(String.format("%.4f  ", this.world.dots[i].bestFitness));
            }
            println();

            this.world.sortDots(false);
            for (int i = 0; i < 10; i++)
            {
                Dot d = this.world.dots[i];
                d.printFitness();
            }
            println();

            println(String.format("best dot's step: %d%s", bestStep, isAtGoal ? "" : " (not at goal)"));

            // genetic algorithm
            this.world.breedNextGeneration();

            println("generation:", this.world.gen);
        }
        else
        {
            // if any of the dots are still alive then update and then show them
            this.world.update();
        }

        this.world.show(this);

        this.saveFrame(String.format("frames/%d/frame#####.tga", this.world.gen));
    }
}
